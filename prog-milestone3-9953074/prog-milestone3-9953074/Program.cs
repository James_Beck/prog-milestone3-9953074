﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace prog_milestone3_9953074
{
    class Program
    {
        static void Main(string[] args)
        {
        //Sets values of variable passed to ProcessOrder Method
        Start:
            int totalcost = 0;

            for (int i = 0; i < i + 1; i++)
            {
                TakeOrder Order = new TakeOrder();
                int costint1 = Order.ProcessOrder(totalcost);

                if (costint1 == 0)
                {
                    Console.Clear();
                    Console.WriteLine("So you didn't mean to order anything, bye!");
                    Thread.Sleep(2000);
                    goto Start;
                }
                else { }

                string coststring = "";
                double costdouble1 = 0.00;

                coststring = costint1.ToString();
                costdouble1 = double.Parse(coststring);

                CustomerDetails Customer1 = new CustomerDetails();
                Customer1.DealingWithCustomer();
                Customer1.CustomersGonPay(costdouble1);

                goto Start;
            }
            //Retrieves values of totalcost for the order from ProcessOrder
            
        }
    }

    public class TakeOrder
    {
        public int ProcessOrder(int totalcost)
        {
            //Perfect marketing for the pizza company
            //Come back and revise this and make it more snazzy
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.BackgroundColor = ConsoleColor.White;
            Console.Clear();

            //Advertisement (so people know where they are)
            Console.WriteLine("||===================================================================================================================||");
            Console.WriteLine("||===================================================================================================================||");
            Console.WriteLine("||                                                                                                                   ||");
            Console.WriteLine("||                                                                                                                   ||");
            Console.WriteLine("||           o   o   o   o                                                                                           ||");
            Console.WriteLine("||          /l  /l  /l  /l                                                                                           ||");
            Console.WriteLine("||         /__l/__l/__l/__l                                                                                          ||");
            Console.WriteLine("||        |                |                                                                                         ||");
            Console.WriteLine("||        | <>  <>  <>  <> |                                                                                         ||");
            Console.WriteLine("||        |________________|                                                            __                           ||");
            Console.WriteLine("||        |______    ______|                                                           |  |                          ||");
            Console.WriteLine("||               |  |                                                                  |__|                          ||");
            Console.WriteLine("||               |  |                                                                                                ||");
            Console.WriteLine("||               |  |        ____________         _____________        _____________         _____________           ||");
            Console.WriteLine("||               |  |       |            |       |   __________|      |    ____     |       |   __________|          ||");
            Console.WriteLine("||               |  |       |     __     |       |  |                 |   |    |    |       |  |                     ||");
            Console.WriteLine("||       _       |  |       |    |  |    |       |  |__________       |   |____|    |       |  |__________           ||");
            Console.WriteLine("||      | |      |  |       |    |  |    |       |__________   |      |   __________|       |__________   |          ||");
            Console.WriteLine("||      | |      |  |       |    |__|    |                  |  |      |  |                             |  |          ||");
            Console.WriteLine("||      | |______|  |       |            |        __________|  |      |  |__________         __________|  |          ||");
            Console.WriteLine("||      |___________|       |____________|       |_____________|      |_____________|       |_____________|          ||");
            Console.WriteLine("||                                                                                                                   ||");
            Console.WriteLine("||                                                                                                                   ||");
            Console.WriteLine("||                                                                                                                   ||");
            Console.WriteLine("||                                                                                                                   ||"); 
            Console.WriteLine("||                || The King of Pizza ||                          || Press Any Key To Order ||                      ||");
            Console.WriteLine("||                                                                                                                   ||");
            Console.WriteLine("||===================================================================================================================||");
            Console.WriteLine("||===================================================================================================================||");

            Console.ReadKey();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();

        repeatinteractioninstructions:
            Console.WriteLine("*****************************************************************************************************************");
            Console.WriteLine("**                                        -->> HOW TO USE TUTORIAL <<--                                        **");
            Console.WriteLine("*****************************************************************************************************************");
            Console.WriteLine("** Here's a little interaction tutorial to get you started                                                     **");
            Console.WriteLine("** Anytime you're posed a question, type in 'y' or 'yes' for YES, everything else will be NO                   **");
            Console.WriteLine("** In a menu, a LIST OF NUMBERS will be on the side, PRESS THE NUMBER that corresponds with you want and ENTER **");
            Console.WriteLine("*****************************************************************************************************************");
            Console.WriteLine();
            Console.WriteLine("Does that make sense?");
            Console.WriteLine("=====================");

            answeringstuff = Console.ReadLine();
            answeringstuff = answeringstuff.ToLower();
            if (answeringstuff == "yes" || answeringstuff == "y")
            { }
            else
            {
                Console.WriteLine("||   Clearly not   ||");
                Console.WriteLine("=====================");
                Thread.Sleep(500);
                Console.Clear();
                goto repeatinteractioninstructions;
            }

            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.BackgroundColor = ConsoleColor.White;
            Console.Clear();

        orderingapizza:
            //Empties the lists that involve toppings the string
            string toppingschange = "";
            currenttoppings.Clear();
            addedtoppings.Clear();
            removedtoppings.Clear();

            Console.Clear();
            Console.WriteLine("=====================================");
            Console.WriteLine("||   So you wanna order a pizza?   ||");
            Console.WriteLine("=====================================");
            Console.WriteLine();
            Console.WriteLine("=============");
            Console.Write("|| Answer: ");

            answeringstuff = Console.ReadLine();
            answeringstuff = answeringstuff.ToLower();
            if (answeringstuff == "yes" || answeringstuff == "y")
            {
                PizzaMenu();
                Base();
            }
            else
            {
                goto nopizzasplease;
            }

            //Pizza with stuffed crust
            Console.Clear();
            Console.WriteLine("==================================");
            Console.WriteLine("||  You wanna stuff the crust?  ||");
            Console.WriteLine("==================================");
            Console.WriteLine();
            Console.WriteLine("=============");
            Console.Write("|| Answer: ");

            answeringstuff = Console.ReadLine();
            answeringstuff = answeringstuff.ToLower();
            if (answeringstuff == "yes" || answeringstuff == "y")
            {
                Stuffing();
            }
            else { }

            //Change pizza toppings
            Current(flavour);
            Console.WriteLine("====================================");
            Console.WriteLine("||  Any changes to the toppings?  ||");
            Console.WriteLine("====================================");
            Console.WriteLine();
            Console.WriteLine("=============");
            Console.Write("|| Answer: ");

            answeringstuff = Console.ReadLine();
            answeringstuff = answeringstuff.ToLower();
            if (answeringstuff == "yes" || answeringstuff == "y")
            {
                Toppings(flavour);
            }
            else { }

            //Pizza size choice
            Size();

            //To summarise possible toppings changes on the pizza
            if (removedtoppings.Count > 0 || addedtoppings.Count > 0)
            {
                toppingschange = "modified";
            }
            else
            {
                toppingschange = "regular";
            }

            Console.Clear();
            //Repeat the order to the customer
            if (pizzamenutype == "Gluten Free")
            {
                //Needs additional information to communicate changes in the toppings
                Console.WriteLine("||   Great, so that's a {0} {1} {2} on a {3} base with {4} stuffed crust for $(5)", size, pizzamenutype, flavour, bread, stuff, cost);
                Console.WriteLine("||");
                foreach (string x in removedtoppings)
                {
                    Console.WriteLine("||   No {0}", x);
                }
                foreach (string x in addedtoppings)
                {
                    Console.WriteLine("||   Extra {0}", x);
                }
            }
            else
            {
                //Needs additional information to communicate changes in the toppings
                
                Console.WriteLine("||   Great, so that's a {0} {1} {2} on a {3} base with {4} stuffed crust for ${5}.   ||", size, flavour, pizzamenutype, bread, stuff, cost);
                Console.WriteLine("||");
                foreach (string x in removedtoppings)
                {
                    Console.WriteLine("||   No {0}", x);
                }
                foreach (string x in addedtoppings)
                {
                    Console.WriteLine("||   Extra {0}", x);
                }
             }

            //Store in a displayable order at the end
            if (size == "small")
            {
                smallorderedpizzas.Add($"{size} {toppingschange} {flavour} {pizzamenutype} with {bread} base and {stuff} stuffed crust");
            }
            else if (size == "medium")
            {
                mediumorderedpizzas.Add($"{size} {toppingschange} {flavour} {pizzamenutype} with {bread} base and {stuff} stuffed crust");
            }
            else
            {
                largeorderedpizzas.Add($"{size} {toppingschange} {flavour} {pizzamenutype} with {bread} base and {stuff} stuffed crust");
            }

            //Would they like to repeat this process and get another pizza?
            Console.WriteLine();
            Console.WriteLine("===================================");
            Console.WriteLine("||   Would like another pizza?   ||");
            Console.WriteLine("===================================");
            Console.WriteLine();
            Console.WriteLine("=============");
            Console.Write("|| Answer: ");

            answeringstuff = Console.ReadLine();
            answeringstuff = answeringstuff.ToLower();
            if (answeringstuff == "yes" || answeringstuff == "y")
            {
                goto orderingapizza;
            }
            else { }

        nopizzasplease:

            //Would they like to add sides to the meal?
            Console.Clear();
            Console.WriteLine("===========================");
            Console.WriteLine("||   Any sides for $2?   ||");
            Console.WriteLine("===========================");
            Console.WriteLine();
            Console.WriteLine("=============");
            Console.Write("|| Answer: ");

            answeringstuff = Console.ReadLine();
            answeringstuff = answeringstuff.ToLower();
            if (answeringstuff == "yes" || answeringstuff == "y")
            {
                Sides();
            }
            else { }

            //Would they like to add desserts to the meal?
            Console.Clear();
            Console.WriteLine("==============================");
            Console.WriteLine("||   Any desserts for $3?   ||");
            Console.WriteLine("==============================");
            Console.WriteLine();
            Console.WriteLine("=============");
            Console.Write("|| Answer: ");

            answeringstuff = Console.ReadLine();
            answeringstuff = answeringstuff.ToLower();
            if (answeringstuff == "yes" || answeringstuff == "y")
            {
                Desserts();
            }
            else { }

        checktheorderbeforeyouwrecktheorder:
            //Check the order

            Console.Clear();
            Console.WriteLine("===========================");
            Console.WriteLine("||   Ok, so your order is: ");
            Console.WriteLine("===========================");
            foreach (string x in smallorderedpizzas)
            {
                Console.WriteLine("||   $9:  {0}", x);
            }
            foreach (string x in mediumorderedpizzas)
            {
                Console.WriteLine("||   $12: {0}", x);
            }
            foreach (string x in largeorderedpizzas)
            {
                Console.WriteLine("||   $14: {0}", x);
            }
            foreach (string x in orderedsides)
            {
                Console.WriteLine("||   $2:  {0}", x);
            }
            foreach (string x in ordereddesserts)
            {
                Console.WriteLine("||   $3:  {0}", x);
            }
   
            totalcost = smallorderedpizzas.Count * 9 + mediumorderedpizzas.Count * 12 + largeorderedpizzas.Count * 14 + orderedsides.Count * 2 + ordereddesserts.Count * 3;
            Console.WriteLine("||");
            Console.WriteLine("||   For a total cost of ${0}", totalcost);

            Console.WriteLine("==========================");
            Console.WriteLine("||   Is this correct?   ||");
            Console.WriteLine("==========================");
            Console.WriteLine();
            Console.WriteLine("=============");
            Console.Write("|| Answer: ");

            answeringstuff = Console.ReadLine();
            answeringstuff = answeringstuff.ToLower();
            if (answeringstuff == "yes" || answeringstuff == "y")
            { }
            else
            {
                CorrectOrder();
                goto checktheorderbeforeyouwrecktheorder;
            }

            orderending();

            return totalcost;
        }

        //Variables to initiate responses
        static string options = "";
        static int response = 0;
        static string answeringstuff = "";

        //String variables that involve characteristics of the pizza
        static string toppingschange = "";
        static string pizzamenutype = "";
        static string flavour = "";
        static string bread = "";
        static string stuff = "no";
        static string size = "";
        static int cost = 0;

        //Tuples for the size and cost values
        static Tuple<string, int> small = Tuple.Create("Small", 9);
        static Tuple<string, int> medium = Tuple.Create("Medium", 12);
        static Tuple<string, int> large = Tuple.Create("Large", 14);

        //Lists manipulate current toppings
        static List<string> currenttoppings = new List<string>();
        static List<string> removedtoppings = new List<string>();
        static List<string> addedtoppings = new List<string>();

        //List to hold the currently ordered pizzas
        static List<string> smallorderedpizzas = new List<string>();
        static List<string> mediumorderedpizzas = new List<string>();
        static List<string> largeorderedpizzas = new List<string>();

        //List to hold the currently ordered sides
        static List<string> orderedsides = new List<string>();

        //List to hold the currently ordered desserts
        static List<string> ordereddesserts = new List<string>();

        //Arrays that store the ingredients of the pizza
        static string[] hawaiianrecipe = new string[] { "Tomato base", "Onion", "Pineapple", "Ham", "Cheese" };
        static string[] vegetarianrecipe = new string[] { "Tomato base", "Onion", "Mushrooms", "Capcicum", "Spinach", "Carrot", "Pineapple" };
        static string[] meatloversrecipe = new string[] { "Tomato base", "Onion", "Bacon", "Beef", "Steak", "Ham", "Sausage" };
        static string[] firebreatherrecipe = new string[] { "Tomato base", "Onion", "Habenero", "Jalepeno", "Orange Sauce" };
        static string[] errythinrecipe = new string[] { "Tomato base", "Onion", "Pineapple", "Ham", "Beef", "Spinach", "Mushrooms", "Sausage" };

        

        public void PizzaMenu()
        {
            pizzamenutype = "";
            flavour = "";
            string holdingmenuwidthleft = "";
            string holdingmenuwidthright = "";

            Console.Clear();
            //Choice to manipulate the type of pizza
            Console.WriteLine("===========================================================");
            Console.WriteLine("||   Would you like a gluten free base for this pizza?   ||");
            Console.WriteLine("===========================================================");
            Console.WriteLine();
            Console.WriteLine("=============");
            Console.Write("|| Answer: ");

            answeringstuff = Console.ReadLine();
            answeringstuff = answeringstuff.ToLower();
            if (answeringstuff == "yes" || answeringstuff == "y")
            {
                pizzamenutype = "Gluten Free Pizza";
            }
            else
            {
                pizzamenutype = "Pizza";
                holdingmenuwidthleft = "      ";
                holdingmenuwidthright = "      ";
            }

            do
            {
                Console.Clear();
                Console.WriteLine("====================================================================================================================");
                Console.WriteLine("||                                          {0}Jose's {1} Menu{2}                                         ||", holdingmenuwidthleft, pizzamenutype, holdingmenuwidthright);
                Console.WriteLine("||================================================================================================================||");
                Console.Write("||   1. Hawaiian!");
                Console.WriteLine("      ---   A delicious combination of juicy pineapple and succulent ham pieces.                 ||");
                Console.Write("||   2. Vegetarian!");
                Console.WriteLine("    ---   A superb combination of mushrooms, onions, capcicum, spinach, carrot, and pineapple. ||");
                Console.Write("||   3. Meatlovers!");
                Console.WriteLine("    ---   A secret, and perfect, ratio of beef, bacon, steak, ham, and sausage.                ||");
                Console.Write("||   4. Fire Breather!");
                Console.WriteLine(" ---   Habenero and Jalepeno peppers, with onion, and our special orange sauce.             ||");
                Console.Write("||   5. Errythin'!");
                Console.WriteLine("     ---   Just a lil' bit of heaven, just a lil' bit of everything.                            ||");
                Console.WriteLine("====================================================================================================================");
                Console.WriteLine();

                Console.WriteLine("=============");
                Console.Write("|| Option: ");
                options = Console.ReadLine();
                int.TryParse(options, out response);
            } while (response > 5 || response < 1);
            
            //Sets the value of variable 'flavour' and adds the ingredients of the corresponding pizza to the toppings list
            switch (response)
            {
                case 1:
                    flavour = "Hawaiian";
                    foreach (string x in hawaiianrecipe)
                    {
                        currenttoppings.Add(x);
                    }
                    currenttoppings.Sort();
                    break;
                case 2:
                    flavour = "Vegetarian";
                    foreach (string x in vegetarianrecipe)
                    {
                        currenttoppings.Add(x);
                    }
                    currenttoppings.Sort();
                    break;
                case 3:
                    flavour = "Meatlovers";
                    foreach (string x in meatloversrecipe)
                    {
                        currenttoppings.Add(x);
                    }
                    currenttoppings.Sort();
                    break;
                case 4:
                    flavour = "Fire Breather";
                    foreach (string x in firebreatherrecipe)
                    {
                        currenttoppings.Add(x);
                    }
                    currenttoppings.Sort();
                    break;
                case 5:
                    flavour = "Errythin";
                    foreach (string x in errythinrecipe)
                    {
                        currenttoppings.Add(x);
                    }
                    currenttoppings.Sort();
                    break;
            }
        }

        public void Base()
        {
            bread = "";

            do
            {
                Console.Clear();
                Console.WriteLine("==============================");
                Console.WriteLine("||   Pizza Base Selection   ||");
                Console.WriteLine("||==========================||");
                Console.WriteLine("||   1. Deep Dish (1 inch)  ||");
                Console.WriteLine("||   2. Halfy (1/2 inch)    ||");
                Console.WriteLine("||   3. Micro (1/4 inch)    ||");
                Console.WriteLine("||   4. Nano (1/16 inch)    ||");
                Console.WriteLine("==============================");
                Console.WriteLine();

                Console.WriteLine("=============");
                Console.Write("|| Option: ");
                options = Console.ReadLine();
                int.TryParse(options, out response);
            } while (response > 4 || response < 1);

            switch (response)
            {
                case 1:
                    bread = "deep dish";
                    break;
                case 2:
                    bread = "halfy";
                    break;
                case 3:
                    bread = "micro";
                    break;
                case 4:
                    bread = "nano";
                    break;
            }
        }

        public void Stuffing()
        {
            stuff = "no";

            do
            {
                Console.Clear();
                Console.WriteLine("==========================");
                Console.WriteLine("||   Stuff Crust Menu   ||");
                Console.WriteLine("||======================||");
                Console.WriteLine("|| 1. Mozarella Cheese  ||");
                Console.WriteLine("|| 2. Sausage           ||");
                Console.WriteLine("|| 3. Macaroni          ||");
                Console.WriteLine("==========================");

                Console.WriteLine("=============");
                Console.Write("|| Option: ");
                options = Console.ReadLine();
                int.TryParse(options, out response);
            } while (response < 1 || response > 3);

            switch (response)
            {
                case 1:
                    stuff = "mozarella Cheese";
                    break;
                case 2:
                    stuff = "sausage";
                    break;
                case 3:
                    stuff = "macaroni";
                    break;
            }
        }

        public void Current(string flavour)
        {
            string menulength = "";

            //Keeps a constant menu length
            if (flavour.Length == 8)
            {
                menulength = "********";
            }
            else if (flavour.Length == 10)
            {
                menulength = "**********";
            }
            else
            {
                menulength = "*************";
            }

            Console.Clear();
            currenttoppings.Sort();

            Console.WriteLine("{0}**************************", menulength);
            Console.WriteLine("Current Pizza Toppings: {0}", flavour);
            Console.WriteLine("{0}**************************", menulength);
            foreach (string x in currenttoppings)
            {
                Console.WriteLine("**  {0}", x);
            }
            Console.WriteLine("{0}**************************", menulength);
        }

        public void Toppings(string flavour)
        {
            //Array with the topping options
            string[] toppingsoptions = new string[] { "Tomato base", "Onion", "Cheese", "Pineapple", "Mushrooms", "Capcicum", "Spinach", "Carrot",
                                                      "Habanero", "Jalepeno", "Ham", "Bacon", "Beef", "Steak", "Sausage", "Orange Sauce" };
            string impossible;

            //Declare index variable for the loops
            int j = 1;

            Current(flavour);
            Console.WriteLine();
            Console.WriteLine("==============================================");
            Console.WriteLine("||   Do you want to remove some toppings?   ||");
            Console.WriteLine("==============================================");
            Console.WriteLine();

            Console.WriteLine("=============");
            Console.Write("|| Answer: ");
            answeringstuff = Console.ReadLine();
            answeringstuff = answeringstuff.ToLower();
            if (answeringstuff == "yes" || answeringstuff == "y")
            {
            removetoppingsswitch:
                //Reset index variable for the loops
                j = 1;

                Current(flavour);
                Console.WriteLine();
                Console.WriteLine("==============================================================");
                Console.WriteLine("||   Type the number to remove the topping to your pizza:   ||");
                Console.WriteLine("||   Type anything else to exit (except spacebar).          ||");
                Console.WriteLine("==============================================================");

                foreach (string x in currenttoppings)
                {
                    Console.WriteLine("||   {0}: {1}", j, x);
                    j++;
                }
                Console.WriteLine("==============================================================");

                //Checks if the number input could even be on the list
                impossible = Console.ReadLine();
                try
                {
                    if (int.Parse(impossible) > currenttoppings.Count())
                    {
                        goto endremovetoppingsswitch;
                    }
                    else { }
                }
                catch
                { }

                switch (impossible)
                {
                    case "1":
                        removedtoppings.Add(currenttoppings[0]);
                        currenttoppings.Remove(currenttoppings[0]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "2":
                        removedtoppings.Add(currenttoppings[1]);
                        currenttoppings.Remove(currenttoppings[1]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "3":
                        removedtoppings.Add(currenttoppings[2]);
                        currenttoppings.Remove(currenttoppings[2]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "4":
                        removedtoppings.Add(currenttoppings[3]);
                        currenttoppings.Remove(currenttoppings[3]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "5":
                        removedtoppings.Add(currenttoppings[4]);
                        currenttoppings.Remove(currenttoppings[4]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "6":
                        removedtoppings.Add(currenttoppings[5]);
                        currenttoppings.Remove(currenttoppings[5]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "7":
                        removedtoppings.Add(currenttoppings[6]);
                        currenttoppings.Remove(currenttoppings[6]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "8":
                        removedtoppings.Add(currenttoppings[7]);
                        currenttoppings.Remove(currenttoppings[7]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "9":
                        currenttoppings.Remove(currenttoppings[8]);
                        removedtoppings.Add(currenttoppings[8]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "10":
                        currenttoppings.Remove(currenttoppings[9]);
                        removedtoppings.Add(currenttoppings[9]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "11":
                        removedtoppings.Add(currenttoppings[10]);
                        currenttoppings.Remove(currenttoppings[10]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "12":
                        removedtoppings.Add(currenttoppings[11]);
                        currenttoppings.Remove(currenttoppings[11]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "13":
                        removedtoppings.Add(currenttoppings[12]);
                        currenttoppings.Remove(currenttoppings[12]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "14":
                        removedtoppings.Add(currenttoppings[13]);
                        currenttoppings.Remove(currenttoppings[13]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "15":
                        removedtoppings.Add(currenttoppings[14]);
                        currenttoppings.Remove(currenttoppings[14]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    case "16":
                        removedtoppings.Add(currenttoppings[15]);
                        currenttoppings.Remove(currenttoppings[15]);
                        Current(flavour);
                        Thread.Sleep(1000);
                        goto removetoppingsswitch;
                    default:
                        break;
                }
            }
            else { }
        endremovetoppingsswitch:

            Console.Clear();
            Current(flavour);
            Console.WriteLine();
            Console.WriteLine("======================================");
            Console.WriteLine("||   Do you want to add toppings?   ||");
            Console.WriteLine("======================================");
            Console.WriteLine();

            Console.WriteLine("=============");
            Console.Write("|| Answer: ");
            
            answeringstuff = Console.ReadLine();
            answeringstuff = answeringstuff.ToLower();
            if (answeringstuff == "yes" || answeringstuff == "y")
            {
            addtoppingsswitch:
                //Reset index variable for the loops
                j = 1;
                Current(flavour);
                Console.WriteLine();
                Console.WriteLine("===========================================================");
                Console.WriteLine("||   Type the number to add the topping to your pizza:   ||");
                Console.WriteLine("||   Type anything else to exit (except spacebar).       ||");
                Console.WriteLine("||   (You can have one of each topping)                  ||");
                Console.WriteLine("===========================================================");

                foreach (string x in toppingsoptions)
                {
                    Console.WriteLine("||   {0}: {1}", j, x);
                    j++;
                }
                switch (Console.ReadLine())
                {
                    case "1":
                        if (currenttoppings.Contains(toppingsoptions[0]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[0]);
                            addedtoppings.Add(toppingsoptions[0]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    case "2":
                        if (currenttoppings.Contains(toppingsoptions[1]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[1]);
                            addedtoppings.Add(toppingsoptions[1]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    case "3":
                        if (currenttoppings.Contains(toppingsoptions[2]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[2]);
                            addedtoppings.Add(toppingsoptions[2]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    case "4":
                        if (currenttoppings.Contains(toppingsoptions[3]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[3]);
                            addedtoppings.Add(toppingsoptions[3]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    case "5":
                        if (currenttoppings.Contains(toppingsoptions[4]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[4]);
                            addedtoppings.Add(toppingsoptions[4]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    case "6":
                        if (currenttoppings.Contains(toppingsoptions[5]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[5]);
                            addedtoppings.Add(toppingsoptions[5]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    case "7":
                        if (currenttoppings.Contains(toppingsoptions[6]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[6]);
                            addedtoppings.Add(toppingsoptions[6]);
                            Current(flavour);
                            goto addtoppingsswitch;
                        }
                    case "8":
                        if (currenttoppings.Contains(toppingsoptions[7]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[7]);
                            addedtoppings.Add(toppingsoptions[7]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    case "9":
                        if (currenttoppings.Contains(toppingsoptions[8]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[8]);
                            addedtoppings.Add(toppingsoptions[8]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    case "10":
                        if (currenttoppings.Contains(toppingsoptions[9]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[9]);
                            addedtoppings.Add(toppingsoptions[9]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    case "11":
                        if (currenttoppings.Contains(toppingsoptions[10]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[10]);
                            addedtoppings.Add(toppingsoptions[10]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    case "12":
                        if (currenttoppings.Contains(toppingsoptions[11]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[11]);
                            addedtoppings.Add(toppingsoptions[11]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    case "13":
                        if (currenttoppings.Contains(toppingsoptions[12]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[12]);
                            addedtoppings.Add(toppingsoptions[12]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    case "14":
                        if (currenttoppings.Contains(toppingsoptions[13]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[13]);
                            addedtoppings.Add(toppingsoptions[13]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    case "15":
                        if (currenttoppings.Contains(toppingsoptions[14]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[14]);
                            addedtoppings.Add(toppingsoptions[14]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    case "16":
                        if (currenttoppings.Contains(toppingsoptions[15]))
                        {
                            Console.WriteLine("Like I said, one of each topping.");
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                        else
                        {
                            currenttoppings.Add(toppingsoptions[15]);
                            addedtoppings.Add(toppingsoptions[15]);
                            Current(flavour);
                            Thread.Sleep(1000);
                            goto addtoppingsswitch;
                        }
                    default:
                        break;
                }
            }
            else { }
        }

        public void Size()
        {
            size = "";
            cost = 0;

            do
            {
                Console.Clear();
                Console.WriteLine("===========================");
                Console.WriteLine("||      Size Choice      ||");
                Console.WriteLine("||=======================||");
                Console.WriteLine("||   1. {0} for ${1}     ||", small.Item1, small.Item2);
                Console.WriteLine("||   2. {0} for ${1}   ||", medium.Item1, medium.Item2);
                Console.WriteLine("||   3. {0} for ${1}    ||", large.Item1, large.Item2);
                Console.WriteLine("===========================");

                Console.WriteLine("=============");
                Console.Write("|| Option: ");
                options = Console.ReadLine();
                int.TryParse(options, out response);
            } while (response > 3 || response < 1);

            switch(response)
            {
                case 1:
                    size = "small";
                    cost = 9;
                    break;
                case 2:
                    size = "medium";
                    cost = 12;
                    break;
                case 3:
                    size = "large";
                    cost = 14;
                    break;
            }
        }

        public void Sides()
        {
            string[] hotsides = new string[] { "Hot Chips", "Buffalo Wings", "Hash Bites", "Onion Rings", "Garlic Bread" };
            string[] coldsides = new string[] { "Coke", "Diet Coke", "Sprite", "Fanta" };

        choosesidesswitch:

            Console.Clear();
            Console.WriteLine("===========================");
            Console.WriteLine("||       Sides Menu      ||");
            Console.WriteLine("||=======================||");
            Console.WriteLine("||   1. Hot Chips        ||");
            Console.WriteLine("||   2. Buffalo Wings    ||");
            Console.WriteLine("||   3. Hash Bites       ||");
            Console.WriteLine("||   4. Onion Rings      ||");
            Console.WriteLine("||   5. Garlic Bread     ||");
            Console.WriteLine("||   6. 1.5L Coke        ||");
            Console.WriteLine("||   7. 1.5L Diet Coke   ||");
            Console.WriteLine("||   8. 1.5L Sprite      ||");
            Console.WriteLine("||   9. 1.5L Fanta       ||");
            Console.WriteLine("===========================");
            Console.WriteLine();

            Console.WriteLine("==============================================");
            Console.WriteLine("||   Type the number of the side you want   ||");
            Console.WriteLine("||   Type anything else to exit the menu    ||");
            Console.WriteLine("==============================================");
            Console.WriteLine();

            Console.WriteLine("=============");
            Console.Write("|| Option: ");

            switch (Console.ReadLine())
            {
                case "1":
                    orderedsides.Add(hotsides[0]);
                    goto choosesidesswitch;
                case "2":
                    orderedsides.Add(hotsides[1]);
                    goto choosesidesswitch;
                case "3":
                    orderedsides.Add(hotsides[2]);
                    goto choosesidesswitch;
                case "4":
                    orderedsides.Add(hotsides[3]);
                    goto choosesidesswitch;
                case "5":
                    orderedsides.Add(hotsides[4]);
                    goto choosesidesswitch;
                case "6":
                    orderedsides.Add(coldsides[0]);
                    goto choosesidesswitch;
                case "7":
                    orderedsides.Add(coldsides[1]);
                    goto choosesidesswitch;
                case "8":
                    orderedsides.Add(coldsides[2]);
                    goto choosesidesswitch;
                case "9":
                    orderedsides.Add(coldsides[3]);
                    goto choosesidesswitch;
                default:
                    break;
            }
        }

        public void Desserts()
        {
            string[] desserts = new string[] { "Chocolate Mousse", "Lava Cake", "Gay Time", "Super Gay Time" };

        choosedessertsswitch:

            Console.Clear();
            Console.WriteLine("=============================");
            Console.WriteLine("||      Desserts Menu      ||");
            Console.WriteLine("=============================");
            Console.WriteLine("||   1. Chocolate Mousse   ||");
            Console.WriteLine("||   2. Lava Cake          ||");
            Console.WriteLine("||   3. Gay Time           ||");
            Console.WriteLine("||   4. Super Gay Time     ||");
            Console.WriteLine("=============================");

            Console.WriteLine();
            Console.WriteLine("===================================================");
            Console.WriteLine("||   Type the number of the dessert you want     ||");
            Console.WriteLine("||   Type anythings else to exit the menu        ||");
            Console.WriteLine("===================================================");
            Console.WriteLine();

            Console.WriteLine("=============");
            Console.Write("|| Option: ");

            switch (Console.ReadLine())
            {
                case "1":
                    ordereddesserts.Add(desserts[0]);
                    goto choosedessertsswitch;
                case "2":
                    ordereddesserts.Add(desserts[1]);
                    goto choosedessertsswitch;
                case "3":
                    ordereddesserts.Add(desserts[2]);
                    goto choosedessertsswitch;
                case "4":
                    ordereddesserts.Add(desserts[3]);
                    goto choosedessertsswitch;
                default:
                    break;
            }
        }

        public void CorrectOrder()
        {
            options = "";
            response = 1000000;

            int i = 1;
            string number;
            string worduse;

            Console.Clear();
            Console.WriteLine("===================================================");
            Console.WriteLine("||     Which part of your order was incorrect    ||");
            Console.WriteLine("||===============================================||");
            Console.WriteLine("||   1. Pizzas                                   ||");
            Console.WriteLine("||   2. Sides                                    ||");
            Console.WriteLine("||   3. Desserts                                 ||");
            Console.WriteLine("||   Any other button. (if there's no problem)   ||");
            Console.WriteLine("===================================================");


            Console.WriteLine("=============");
            Console.Write("|| Option: ");
            switch (Console.ReadLine())
            {
                case "1":
                    Console.Clear();
                    Console.WriteLine("================================================");
                    Console.WriteLine("||        What's wrong with the pizza?        ||");
                    Console.WriteLine("||============================================||");
                    Console.WriteLine("||   1. I don't want one/some of the pizzas   ||");
                    Console.WriteLine("||   2. One of the pizzas is incorrect        ||");
                    Console.WriteLine("||   3. I want to add a pizza to my order     ||");
                    Console.WriteLine("================================================");
                    Console.WriteLine();

                    Console.WriteLine("=============");
                    Console.Write("|| Option: ");
                    number = Console.ReadLine();
                    if (number == "1")
                    {
                        worduse = "delete";
                    }
                    else
                    {
                        worduse = "change";
                    }

                    switch (number)
                    {
                        case "1":
                            do
                            {
                                Console.Clear();
                                if (smallorderedpizzas.Count() + mediumorderedpizzas.Count() + largeorderedpizzas.Count() == 0)
                                {
                                    Console.WriteLine("===============================================================");
                                    Console.WriteLine("||   You can't {0} pizzas that you haven't selected", worduse);
                                    Console.WriteLine("===============================================================");
                                    Thread.Sleep(1000);
                                    goto default;
                                }
                                else { }

                                i = 1;
                                foreach (string x in smallorderedpizzas)
                                {
                                    Console.WriteLine("   {0}. {1}", i, x);
                                    i++;
                                }
                                foreach (string x in mediumorderedpizzas)
                                {
                                    Console.WriteLine("   {0}. {1}", i, x);
                                    i++;
                                }
                                foreach (string x in largeorderedpizzas)
                                {
                                    Console.WriteLine("   {0}. {1}", i, x);
                                    i++;
                                }
                                Console.WriteLine("Select the pizza you would like to {0}", worduse);

                                Console.WriteLine("=============");
                                Console.Write("|| Option: ");
                                options = Console.ReadLine();
                                int.TryParse(options, out response);
                            } while (response > smallorderedpizzas.Count + mediumorderedpizzas.Count + largeorderedpizzas.Count || response < 1);

                            if (response < smallorderedpizzas.Count)
                            {
                                smallorderedpizzas.Remove(smallorderedpizzas[response - 1]);
                            }
                            else if (response < smallorderedpizzas.Count + mediumorderedpizzas.Count && response > smallorderedpizzas.Count)
                            {
                                mediumorderedpizzas.Remove(mediumorderedpizzas[response - smallorderedpizzas.Count - 1]);
                            }
                            else
                            {
                                largeorderedpizzas.Remove(largeorderedpizzas[response - smallorderedpizzas.Count - mediumorderedpizzas.Count - 1]);
                            }

                            if (worduse == "change")
                            {
                                PizzaMenu();
                                Base();
                                Stuffing();
                                Toppings(flavour);
                                Size();
                                goto default;
                            }
                            else { }

                            Console.Clear();
                            Console.WriteLine("Any other pizzas to get rid of?");

                            answeringstuff = Console.ReadLine();
                            answeringstuff = answeringstuff.ToLower();
                            if (answeringstuff == "yes" || answeringstuff == "y")
                            {
                                goto case "1";
                            }
                            else { }

                            break;
                        case "2":
                            goto case "1";
                        case "3":
                            PizzaMenu();
                            Base();
                            Stuffing();
                            Toppings(flavour);
                            Size();
                            if (size == "small")
                            {
                                smallorderedpizzas.Add($"{size} {toppingschange} {flavour} {pizzamenutype} with {bread} base and {stuff} stuffed crust");
                            }
                            else if (size == "medium")
                            {
                                mediumorderedpizzas.Add($"{size} {toppingschange} {flavour} {pizzamenutype} with {bread} base and {stuff} stuffed crust");
                            }
                            else
                            {
                                largeorderedpizzas.Add($"{size} {toppingschange} {flavour} {pizzamenutype} with {bread} base and {stuff} stuffed crust");
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case "2":
                    Console.Clear();
                    Console.WriteLine("===============================================");
                    Console.WriteLine("||        What's wrong with the sides?       ||");
                    Console.WriteLine("||===========================================||");
                    Console.WriteLine("||   1. I don't want one/some of the sides   ||");
                    Console.WriteLine("||   2. I want to change what I've ordered   ||");
                    Console.WriteLine("||   3. I want to add some more sides        ||");
                    Console.WriteLine("===============================================");
                    Console.WriteLine();

                    Console.WriteLine("=============");
                    Console.Write("|| Option: ");
                    number = Console.ReadLine();
                    if (number == "1")
                    {
                        worduse = "delete";
                    }
                    else
                    {
                        worduse = "change";
                    }

                    switch (number)
                    {
                        case "1":
                            do
                            {
                                Console.Clear();
                                if (orderedsides.Count() == 0)
                                {
                                    Console.WriteLine("===============================================================");
                                    Console.WriteLine("You can't {0} sides that you haven't selected", worduse);
                                    Console.WriteLine("===============================================================");
                                    Thread.Sleep(1000);
                                    goto default;
                                }
                                else { }                                

                                i = 1;
                                foreach (string x in orderedsides)
                                {
                                    Console.WriteLine("   {0}. {1}", i, x);
                                    i++;
                                }
                                Console.WriteLine("Select the side(s) you would like to {0}", worduse);

                                Console.WriteLine("=============");
                                Console.Write("|| Option: ");
                                options = Console.ReadLine();
                                int.TryParse(options, out response);
                            } while (response > orderedsides.Count || response < 1);

                            orderedsides.Remove(orderedsides[response - 1]);

                            if (worduse == "change")
                            {
                                Sides();
                                goto default;
                            }
                            else { }

                            Console.Clear();
                            Console.WriteLine("Any other sides to get rid of?");

                            answeringstuff = Console.ReadLine();
                            answeringstuff = answeringstuff.ToLower();
                            if (answeringstuff == "yes" || answeringstuff == "y")
                            {
                                goto case "1";
                            }

                            break;
                        case "2":
                            goto case "1";
                        case "3":
                            Sides();
                            break;
                        default:
                            break;
                    }
                    break;
                case "3":
                    Console.Clear();
                    Console.WriteLine("==================================================");
                    Console.WriteLine("||        What's wrong with the desserts?       ||");
                    Console.WriteLine("||==============================================||");
                    Console.WriteLine("||   1. I dont' want one/some of the desserts   ||");
                    Console.WriteLine("||   2. I want to change what I've ordered      ||");
                    Console.WriteLine("||   3. I want to add some more desserts        ||");
                    Console.WriteLine("==================================================");
                    Console.WriteLine();

                    Console.WriteLine("=============");
                    Console.Write("|| Option: ");
                    number = Console.ReadLine();
                    if (number == "1")
                    {
                        worduse = "delete";
                    }
                    else
                    {
                        worduse = "change";
                    }

                    switch (number)
                    {
                        case "1":
                            do
                            {
                                Console.Clear();
                                if (ordereddesserts.Count() == 0)
                                {
                                    Console.WriteLine("===============================================================");
                                    Console.WriteLine("You can't {0} desserts that you haven't selected", worduse);
                                    Console.WriteLine("===============================================================");
                                    Thread.Sleep(1000);
                                    goto default;
                                }
                                else { }
                                i = 1;
                                foreach (string x in ordereddesserts)
                                {
                                    Console.WriteLine("   {0}. {1}", i, x);
                                    i++;
                                }
                                Console.WriteLine("Select the dessert(s) you would like to {0}", worduse);

                                Console.WriteLine("=============");
                                Console.Write("|| Option: ");
                                options = Console.ReadLine();
                                int.TryParse(options, out response);
                            } while (response > ordereddesserts.Count || response < 1);

                            ordereddesserts.Remove(ordereddesserts[response - 1]);

                            if (worduse == "change")
                            {
                                Desserts();
                                goto default;
                            }
                            else { }

                            Console.Clear();
                            Console.WriteLine("Any other desserts to get rid of?");

                            answeringstuff = Console.ReadLine();
                            answeringstuff = answeringstuff.ToLower();
                            if (answeringstuff == "yes" || answeringstuff == "y")
                            {
                                goto case "1";
                            }
                            else { }

                            break;
                        case "2":
                            goto case "1";
                        case "3":
                            Desserts();
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

            public void orderending()
        {
            ordereddesserts.Clear();
            orderedsides.Clear();
            largeorderedpizzas.Clear();
            mediumorderedpizzas.Clear();
            smallorderedpizzas.Clear();
            addedtoppings.Clear();
            removedtoppings.Clear();
            currenttoppings.Clear();
        }
    }

    public class CustomerDetails
    {
        public void DealingWithCustomer()
        {
        anameplease:

            Console.Clear();
            Console.WriteLine("=====================================================================");
            Console.WriteLine("||   Great, so now that order's complete, lets get some details.   ||");
            Console.WriteLine("=====================================================================");
            Console.WriteLine();

            Console.WriteLine("********************************");
            Console.WriteLine("**   What's your full name?   **");
            Console.WriteLine("********************************");
            Console.WriteLine();

            Console.WriteLine("===========================================================");
            Console.Write("||   Your full name please:  ");
            customername = Console.ReadLine();
            if (customername.Length == 0)
            {
                goto anameplease;
            }
            else { }
            customername.ToLower();

            //Testing whether we've seen this customer before
            if (customerdatabase.ContainsKey(customername))
            {
                Console.WriteLine("===================================================================");
                Console.WriteLine("||   It's nice to see you again {0}", customername);
                Thread.Sleep(1000);
                talkingtocustomer = customername;                         

                goto wehaveseenthiscustomerbefore;
            }
            else
            {
                do
                {
                    Console.Clear();
                    Console.WriteLine("================================================================");
                    Console.Write("||   Great, and your cellphone details too please:  ");
                    options = Console.ReadLine();
                    int.TryParse(options, out response);

                    //removes the value change of response if the cellphone number is too short or long
                    if (options.Length > 12 || options.Length < 9)
                    {
                        response = 0;

                        Console.WriteLine();
                        Console.WriteLine("***********************************************************************************");
                        Console.WriteLine("**  New Zealand cellphone numbers are between 9 and 12 characters integers long  **");
                        Console.WriteLine("***********************************************************************************");
                        Thread.Sleep(1000);
                    }
                    else { }

                } while (options.Length < 9 || options.Length > 12 || response == 0);

                customerdatabase.Add(customername, response);
            }

            customername = "";
            customernumber = 0;
        wehaveseenthiscustomerbefore:;
        }

        //database to store customer information
        public static Dictionary<string, int> customerdatabase = new Dictionary<string, int>();

        //Store current customer details
        public static string customername = "";
        public static int customernumber = 0;
        public static string talkingtocustomer = "";

        //For the do-while game
        public static string options = "";
        public static int response = 0;
        public static string answeringstuff = "";
        public static double cashover = 0.00;

        public void CustomersGonPay(double costdouble1)
        {
        paymore:

            string borderlengthname = "";
            string borderlengthcost = "";
            string borderlengthblanklong = "";
            string borderlengthblankshort = "";
            string namelength = talkingtocustomer;
            string costlength = costdouble1.ToString();

            namelength.ToCharArray();
            costlength.ToCharArray();

            foreach (char x in namelength)
            {
                borderlengthname = borderlengthname + "=";
                borderlengthblanklong = borderlengthblanklong + " ";
                borderlengthblankshort = borderlengthblankshort + " ";
            }

            foreach (char x in costlength)
            {
                borderlengthcost = borderlengthcost + "=";
            }

            do
            {
                cashover = 0.00;
                Console.Clear();
                Console.WriteLine("===================================={0}{1}", borderlengthcost, borderlengthname);
                Console.WriteLine("||   Alright {0}, the damage is ${1}.   ||", talkingtocustomer, costdouble1);
                Console.WriteLine("===================================={0}{1}", borderlengthcost, borderlengthname);
                Console.WriteLine();

                Console.WriteLine("**********************************************************");
                Console.WriteLine("**   Type in the cash amount you're going to transfer   **");
                Console.WriteLine("**********************************************************");
                Console.WriteLine();

                Console.WriteLine("=============");
                Console.Write("|| $ ");
                options = Console.ReadLine();
                double.TryParse(options, out cashover);

            } while (cashover <= 0);

            costdouble1 = costdouble1 - cashover;

            borderlengthcost = "";
            costlength = costdouble1.ToString();
            costlength.ToCharArray();
            foreach (char x in costlength)
            {
                borderlengthcost = borderlengthcost + "=";
                borderlengthblanklong = borderlengthblanklong + " ";
            }

            do
            {
                int i = 0;
                if (0 > costdouble1)
                {
                    do
                    {
                        Console.Clear();
                        Console.WriteLine("{0}{1}===========================================================", borderlengthcost, borderlengthname);
                        Console.WriteLine("||   Awesome, here's you're ${0} change, have a nice day {1}!   ||", costdouble1 * -1, talkingtocustomer);
                        Console.WriteLine("||   Press ENTER to EXIT     {0}                            ||", borderlengthblanklong);
                        Console.WriteLine("{0}{1}===========================================================", borderlengthcost, borderlengthname);
                        Console.WriteLine();
                    } while (Console.ReadKey().Key != ConsoleKey.Enter);
                }
                else if (0 == costdouble1)
                {
                    do
                    {
                        Console.Clear();
                        Console.WriteLine("{0}====================================", borderlengthname);
                        Console.WriteLine("||   Awesome, have a nice day {0}!   ||", talkingtocustomer);
                        Console.WriteLine("||   Press ENTER to EXIT{0}          ||", borderlengthblankshort);
                        Console.WriteLine("{0}====================================", borderlengthname);
                        Console.WriteLine();
                    } while (Console.ReadKey().Key != ConsoleKey.Enter);
                }
                else if (0 < costdouble1 && i == 0)
                {
                    Console.Clear();
                    Console.WriteLine("======================================================");
                    Console.WriteLine("||   Ooh, seems like you're a little short there.   ||");
                    Console.WriteLine("||   Do you have anymore cash on you?               ||");
                    Console.WriteLine("======================================================");
                    Console.WriteLine();
                    Console.WriteLine("=============");
                    Console.Write("|| Answer: ");

                    answeringstuff = Console.ReadLine();
                    answeringstuff = answeringstuff.ToLower();
                    if (answeringstuff == "yes" || answeringstuff == "y")
                    {
                        goto paymore;
                    }
                    else if (costdouble1 <= 0.5)
                    {
                        Console.Clear();
                        Console.WriteLine("{0}===============================================================", borderlengthcost);
                        Console.WriteLine("||   It's all goods bro, don't sweat the ${0} have a good day   ||", costdouble1);
                        Console.WriteLine("||   Press ANY KEY to EXIT{0}                                  ||", borderlengthblankshort);
                        Console.WriteLine("{0}===============================================================", borderlengthcost);
                        costdouble1 = 0;
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("========================================================");
                        Console.WriteLine("||   Alright then, bye, we have to serve a customer   ||");
                        Console.WriteLine("========================================================");
                        Console.ReadKey();
                        costdouble1 = 0;
                    }
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("=========================================================");
                    Console.WriteLine("||   Oh bro, seems like you're still a little short.   ||");
                    Console.WriteLine("||   Do you have anymore cash on you?                  ||");
                    Console.WriteLine("=========================================================");
                    Console.WriteLine();
                    Console.WriteLine("=============");
                    Console.Write("|| Answer: ");

                    answeringstuff = Console.ReadLine();
                    answeringstuff = answeringstuff.ToLower();
                    if (answeringstuff == "yes" || answeringstuff == "y")
                    {
                        goto paymore;
                    }
                    else if (costdouble1 - cashover <= 0.5)
                    {
                        Console.Clear();
                        Console.WriteLine("{0}===============================================================", borderlengthcost);
                        Console.WriteLine("||   It's all goods bro, don't sweat the ${0} have a good day   ||", costdouble1);
                        Console.WriteLine("||   Press ENTER to EXIT{0}                                    ||", borderlengthblankshort);
                        Console.WriteLine("{0}===============================================================", borderlengthcost);
                        costdouble1 = 0;
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("========================================================");
                        Console.WriteLine("||   Alright then, bye, we have to serve a customer   ||");
                        Console.WriteLine("========================================================");
                        Console.ReadKey();
                        costdouble1 = 0;
                    }
                }
            } while (costdouble1 > 0);

        }
    }
}

